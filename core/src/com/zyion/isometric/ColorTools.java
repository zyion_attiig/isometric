package com.zyion.isometric;

/**
 * Created by Zyion on 9/01/2016.
 */
public class ColorTools {

    public static String getColorString(int colorValue) {
        int[] color = new int[4];
        color[0] = (colorValue >> 24) & 0xff;
        color[1] = (colorValue >> 16) & 0xff;
        color[2] = (colorValue >> 8) & 0xff;
        color[3] = colorValue & 0xff;
        return String.format("#%02x%02x%02x", color[0], color[1], color[2]);
    }

}
