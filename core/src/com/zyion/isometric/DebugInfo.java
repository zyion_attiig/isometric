package com.zyion.isometric;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class DebugInfo {

    private Label lblFps, lblMouse, lblMouseIso;
    
	public DebugInfo(Stage stage, BitmapFont font) {
		
        Label.LabelStyle lblStyle = new Label.LabelStyle(font, Color.BLUE);
        lblFps = new Label("FPS: ", lblStyle);
        lblFps.setPosition(10, 10);
        stage.addActor(lblFps);

        lblMouse = new Label("Mouse: ", lblStyle);
        lblMouse.setPosition(10, 30);
        stage.addActor(lblMouse);

        lblMouseIso = new Label("Mouse ISO: ", lblStyle);
        lblMouseIso.setPosition(10, 50);
        stage.addActor(lblMouseIso);
        
	}
	
	public void update(float mouseX, float mouseY, int tileX, int tileY) {
        lblFps.setText("FPS: " + Gdx.graphics.getFramesPerSecond());
        lblMouse.setText("Mouse x: " + mouseX + " y: " + mouseY);
        lblMouseIso.setText("Mouse ISO x: " + tileX + " y: " + tileY);
	}

}
