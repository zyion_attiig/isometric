package com.zyion.isometric;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Zyion on 20/12/2015.
 */
public class InputHandler implements InputProcessor, GestureDetector.GestureListener {

    private OrthographicCamera camera;
    private Vector3 mousePos;
    private Vector2 mouse;
    private HashMap<Integer, Boolean> keys;

    private boolean leftMouseButton = false, rightMouseButton = false;

    public InputHandler(OrthographicCamera camera) {
        this.camera = camera;
        this.mousePos = new Vector3(0, 0, 0);
        this.mouse = new Vector2(0, 0);
        this.keys = new HashMap<Integer, Boolean>();
    }

    public Vector2 getMousePosition() {
        return mouse;
    }

    public boolean isLeftMouseButtonDown() {
        return leftMouseButton;
    }

    public boolean isRightMouseButtonDown() {
        return rightMouseButton;
    }

    // Input Processor

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        mousePos.set(screenX, screenY, 0);
        camera.unproject(mousePos);
        mouse.set(mousePos.x, mousePos.y);
        switch (button) {
            case 0:
                leftMouseButton = true;
                break;
            case 1:
                rightMouseButton = true;
                break;
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        mousePos.set(screenX, screenY, 0);
        camera.unproject(mousePos);
        mouse.set(mousePos.x, mousePos.y);
        switch (button) {
            case 0:
                leftMouseButton = false;
                break;
            case 1:
                rightMouseButton = false;
                break;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        mousePos.set(screenX, screenY, 0);
        camera.unproject(mousePos);
        mouse.set(mousePos.x, mousePos.y);
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        mousePos.set(screenX, screenY, 0);
        camera.unproject(mousePos);
        mouse.set(mousePos.x, mousePos.y);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if (amount > 0 && camera.zoom < 2) {
            camera.zoom += .1f;
        }
        if (amount < 0 && camera.zoom > .6f) {
            camera.zoom -= .1f;
        }
        Gdx.app.log("Zoom", " " + camera.zoom);
        return false;
    }

    // Gesture Listener

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

}
