package com.zyion.isometric;

import java.util.HashMap;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.zyion.isometric.map.Character;
import com.zyion.isometric.map.MapObjectFactory;
import com.zyion.isometric.map.TileMap;
import com.zyion.isometric.map.TileMapRenderer;

public class IsoGame extends ApplicationAdapter {

    public static final String TITLE = "Extrication";
    public static final int WIDTH = 800, HEIGHT = 600;
    public static boolean showDebug = false;
    private boolean running = false;
    
    private XMLAssetManager assets;
    private Stage stage;
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private StretchViewport viewport;
    private InputHandler input;
    private TextDisplay textDisplay;
    private DebugInfo debug;
    
    private HashMap<String, Pixmap> maps = new HashMap<String, Pixmap>();
    private TileMapRenderer mapRenderer;
    private TileMap map;
    private Character playerObject;
    
    private Menu menu;
    
    Texture selectionMask;

    @Override
    public void create() {
        assets = new XMLAssetManager().loadXML(Gdx.files.internal("Assets.xml"));
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new StretchViewport(WIDTH, HEIGHT, camera);
        input = new InputHandler(camera);
        stage = new Stage(new StretchViewport(WIDTH, HEIGHT));
        Gdx.input.setInputProcessor(new InputMultiplexer(stage, input));
        mapRenderer = new TileMapRenderer(assets);
        
        menu = new Menu(assets.getSkin("gameskin"), new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (actor.getName().equalsIgnoreCase("BTN_TITLE")) {
					startGame();
				}
			}
        });

        // maps
        maps.put("cryogenic", assets.getPixmap("cryogenic"));
        maps.put("cryogenic_safety_blocker", assets.getPixmap("cryogenic_safety_blocker"));
        maps.put("engine_maintenance_room", assets.getPixmap("engine_maintenance_room"));
        maps.put("four_way_connection_room", assets.getPixmap("four_way_connection_room"));
        maps.put("main_engine_room", assets.getPixmap("main_engine_room"));
        maps.put("safe_area", assets.getPixmap("safe_area"));

        maps.put("habitat_corridor", assets.getPixmap("habitat_corridor"));
        maps.put("escape_corridor", assets.getPixmap("escape_corridor"));
        maps.put("cockpit", assets.getPixmap("cockpit"));
        maps.put("common_room", assets.getPixmap("common_room"));
        maps.put("left_cannon_room", assets.getPixmap("left_cannon_room"));
        maps.put("left_engine_room", assets.getPixmap("left_engine_room"));
        maps.put("right_cannon_room", assets.getPixmap("right_cannon_room"));
        maps.put("right_engine_room", assets.getPixmap("right_engine_room"));
        maps.put("control_room", assets.getPixmap("control_room"));
        
        selectionMask = assets.getTexture("selection_mask");

        showMenu();
        
        if (showDebug) debug = new DebugInfo(stage, new BitmapFont());
    }
    
    public void startGame() {
    	menu.remove();
        // create map
    	MapObjectFactory mapObjectFactory = new MapObjectFactory();
        map = new TileMap(maps, mapObjectFactory);
        map.loadMap("cryogenic");
        // player object
        playerObject = new Character(map, 4, 4, 0);
        map.addObject(playerObject);
        // text display
        textDisplay = new TextDisplay(assets.getSkin("gameskin"));
        stage.addActor(textDisplay);
        // load map
        mapObjectFactory.loadMap(map, "cryogenic", textDisplay, playerObject);
    	running = true;
    }
    
    public void showMenu() {
    	if (textDisplay != null) {
    		textDisplay.remove();
    		textDisplay = null;
    	}
    	running = false;
    	stage.addActor(menu);
    }


    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (running)
        	renderGame();

        stage.act();
        stage.draw();
    }
    
    public void renderGame() {
        // mouse positioning
        Vector2 mouse = input.getMousePosition();
        Vector2 mouseIso = TileMap.from(mouse.x, mouse.y);
        int selectedTileX = (int) Math.floor(mouseIso.x / map.size);
        int selectedTileY = (int) Math.floor(mouseIso.y / map.size);
        Vector2 selectedTilePos = map.getTileCenterPosition(selectedTileX, selectedTileY);

        map.update();
        map.updateInput(input, textDisplay, playerObject);

        camera.position.set((int) Math.floor(playerObject.position.x), (int) Math.floor(playerObject.position.y), 0);
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        mapRenderer.renderMap(batch, map);

        // render cell selection
        int blendDst = batch.getBlendDstFunc();
        int blendSrc = batch.getBlendSrcFunc();
        Color color = batch.getColor();
        batch.setColor(Color.SKY);
        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        batch.draw(selectionMask, selectedTilePos.x - 32, selectedTilePos.y - 16);
        batch.setColor(color);
        batch.setBlendFunction(blendSrc, blendDst);

        batch.end();
        textDisplay.update();
        
        // update debugging
        if (showDebug) debug.update(mouse.x, mouse.y, selectedTileX, selectedTileY);
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        //camera.setToOrtho(false, width, height);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        assets.unloadXML(Gdx.files.internal("Assets.xml"));
        assets.dispose();
        batch.dispose();
        stage.dispose();
    }

}
