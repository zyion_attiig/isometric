package com.zyion.isometric;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class Menu extends Table {

	private Label lblTitle;
	private TextButton btnStartGame;
	
	public Menu(Skin skin, ChangeListener listener) {
        setFillParent(true);
        // title
        lblTitle = new Label(IsoGame.TITLE, skin, "title");
        lblTitle.setFontScale(1.5f);
        add(lblTitle).pad(20).row();
        // start game button
        btnStartGame = new TextButton("Start Game", skin);
        btnStartGame.setName("BTN_TITLE");
        btnStartGame.addListener(listener);
        add(btnStartGame).width(200).height(100).pad(10);
	}

}
