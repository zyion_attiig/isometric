package com.zyion.isometric;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Zyion on 22/12/2015.
 */
public class Spritesheet {

    private Texture texture;
    private int spriteWidth, spriteHeight;

    public Spritesheet(Texture texture, int spriteWidth, int spriteHeight) {
        this.texture = texture;
        this.spriteWidth = spriteWidth;
        this.spriteHeight = spriteHeight;
    }

    public TextureRegion getSprite(int x, int y) {
    	return new TextureRegion(texture, x * spriteWidth, y * spriteHeight, spriteWidth, spriteHeight);
    }
}
