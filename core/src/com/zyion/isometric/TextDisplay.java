package com.zyion.isometric;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * Created by Zyion on 11/01/2016.
 */
public class TextDisplay extends Table {

    private Label textDisplay;
    private TextButton btnSkip;
    private LinkedList<String> conversation = new LinkedList<String>();
    public static final int DISPLAY_TIME = 180;
    private int textTicks = 0;
    private float fontScale = 1.6f;
    
    public boolean displaying = false;

    public TextDisplay(Skin skin) {
        setFillParent(true);
        
        btnSkip = new TextButton("Skip", skin);
        btnSkip.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.log("Skippy", "fucker");
				skip();
			}
		});
        add(btnSkip).right().pad(10).row();
        
        textDisplay = new Label(" ", skin);
        textDisplay.setFontScale(fontScale);
        add(textDisplay).expand().bottom().pad(20);
    }


    public boolean isOnDisplay() {
        if (textTicks > 0 || conversation.size() > 0) return true;
        else return false;
    }

    public void display(String[] text) {
        for (int i = 0; i < text.length; i++) {
            conversation.add(text[i]);
        }
    }

    public void display(String text) {
        conversation.add(text);
    }
    
    public void skip() {
    	conversation.clear();
    	displaying = false;
    	setVisible(displaying);
    	textTicks = 0;
    }

    public void update() {
        if (textTicks == 1) {
            textDisplay.setText("");
            textTicks--;
        } else if (textTicks > 0) {
            textTicks--;
        } else if (conversation.size() > 0) {
            textDisplay.setText(conversation.removeFirst());
            textTicks = DISPLAY_TIME;
            displaying = true;
            setVisible(displaying);
        } else {
        	displaying = false;
        	setVisible(displaying);
        }
    }

}
