package com.zyion.isometric;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.assets.loaders.SkinLoader.SkinParameter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

/**
 * 
 * @author Zyion <h1>XML Asset Manager</h1>
 *         <p>
 *         Loads resources from XML file maintains assets with AssetManager
 *         import com.badlogic.gdx.assets.AssetManager;
 *
 *         <h2>Asset Manager Info</h2>
 *         </br> description: <a
 *         href="https://github.com/libgdx/libgdx/wiki/Managing-your-assets"
 *         >Asset Manager Information</a> </br> source: <a
 *         href="https://github.com/libgdx/libgdx/wiki/Managing-your-assets"
 *         >Asset Manager Source</a>
 *
 *         <h2>XML format - Code Sample
 *         <h2/>
 *         <code><pre>
 * store assets inside a root XNL element
 * id values are used for later referencing
 * {@code
 * 	<Texture id="badlogic">data/badlogic.jpg</Texture>
 * }
 * </code></pre>
 *         </P>
 */
public class XMLAssetManager extends AssetManager {
	private ObjectMap<String, String> assets = new ObjectMap<String, String>();

	/**
	 * Load XML file Specify the XML file with game asset definitions
	 * 
	 * @param path
	 *            XML file location Gdx.files.internal("data/yourAssets.xml")
	 * @return this instance
	 */
	public XMLAssetManager loadXML(FileHandle path) {
		try {
			loadChildren(new XmlReader().parse(path));
			finishLoading();
		} catch (IOException e) {
			Gdx.app.error("Could not open file", path.path());
		}
		return this;
	}

	/**
	 * iterates through XML elements inside the root element passes each element
	 * to the load assets method
	 * 
	 * @param root
	 *            XML element
	 */
	private void loadChildren(Element root) {
		for (int i = 0; i < root.getChildCount(); i++)
			loadAsset(root.getChild(i));
	}

	/**
	 * checks XML elements for assets to be loaded and stored into the asset
	 * manager
	 * 
	 * @param element
	 *            The XML element with asset data
	 */
	private void loadAsset(Element element) {
		String atlas = null;
		if (element.getAttributes() != null) {
			ObjectMap<String, String> attributes = element.getAttributes();
			for (String s : attributes.keys()) {
				if (s.equals("id"))
					assets.put(element.get("id"), element.getText());
				else if (s.equals("atlas"))
					atlas = element.get("atlas");
			}
			attributes.clear();
		}

		if (element.getName().equals("BitmapFont"))
			load(element.getText(), BitmapFont.class);

		else if (element.getName().equals("Music"))
			load(element.getText(), Music.class);

		else if (element.getName().equals("ParticleEffect"))
			load(element.getText(), ParticleEffectLoader.class);

		else if (element.getName().equals("Pixmap"))
			load(element.getText(), Pixmap.class);

		else if (element.getName().equals("Skin")) {
			if (atlas != null)
				load(element.getText(), Skin.class, new SkinParameter(checkForAsset(atlas)));
			else
				load(element.getText(), Skin.class);
		}

		else if (element.getName().equals("Sound"))
			load(element.getText(), Sound.class);

		else if (element.getName().equals("Texture"))
			load(element.getText(), Texture.class);

		else if (element.getName().equals("TextureAtlas"))
			load(element.getText(), TextureAtlas.class);

		else if (element.getName().equals("TiledMap")) {
			setLoader(TiledMap.class, new TmxMapLoader());
			load(element.getText(), TiledMap.class);
		}

		Gdx.app.log(element.getName(), element.getText());
	}

	/**
	 * Unload XML file Specify the XML file with game asset definitions
	 * 
	 * @param path
	 *            XML file location Gdx.files.internal("data/yourAssets.xml")
	 * @return this instance
	 */
	public XMLAssetManager unloadXML(FileHandle path) {
		try {
			unloadChildren(new XmlReader().parse(path));
			finishLoading();
		} catch (IOException e) {
			Gdx.app.error("Could not open file", path.path());
		}
		return this;
	}

	/**
	 * Unload Children Iterates through all child assets and passes them to the
	 * unload asset method
	 * 
	 * @param root
	 *            XML root element
	 */
	private void unloadChildren(Element root) {
		for (int i = 0; i < root.getChildCount(); i++)
			unloadAsset(root.getChild(i));
	}

	/**
	 * Unload Asset XML with asset asset information will be removed from the
	 * asset manager
	 * 
	 * @param element
	 *            XML element to remove from assets
	 */
	private void unloadAsset(Element element) {
		if (element.getAttributes() != null) {
			ObjectMap<String, String> attributes = element.getAttributes();
			for (String s : attributes.keys()) {
				if (s.equals("id"))
					assets.remove(element.getText());
				else if (s.equals("atlas"))
					unload(element.get("atlas"));
			}
			attributes.clear();
		}
		if (containsAsset(element.getText()))
			unload(element.getText());

		Gdx.app.log(element.getName(), element.getText());
	}

	/**
	 * Get a BitmapFont from the asset manager stored via the XML id value
	 * 
	 * @param id
	 * @return BitmapFont with id
	 */
	public BitmapFont getBitmapFont(String id) {
		return get(assets.get(id), BitmapFont.class);
	}

	/**
	 * Get a Music track from the asset manager stored via the XML id value
	 * 
	 * @param id
	 * @return Music with id
	 */
	public Music getMusic(String id) {
		return get(assets.get(id), Music.class);
	}

	/**
	 * Get a ParticleEffectLoader from the asset manager stored via the XML id
	 * value
	 * 
	 * @param id
	 * @return ParticleEffectLoader with id
	 */
	public ParticleEffectLoader getParticleEffect(String id) {
		return get(assets.get(id), ParticleEffectLoader.class);
	}

	/**
	 * Get a Pixmap from the asset manager stored via the XML id value
	 * 
	 * @param id
	 * @return Pixmap with id
	 */
	public Pixmap getPixmap(String id) {
		return get(assets.get(id), Pixmap.class);
	}

	/**
	 * Get a Skin from the asset manager stored via the XML id value
	 * 
	 * @param id
	 * @return Skin with id
	 */
	public Skin getSkin(String id) {
		return get(assets.get(id), Skin.class);
	}

	/**
	 * Get a Sound from the asset manager stored via the XML id value
	 * 
	 * @param id
	 * @return Sound with id
	 */
	public Sound getSound(String id) {
		return get(assets.get(id), Sound.class);
	}

	/**
	 * Get a Texture from the asset manager stored via the XML id value
	 * 
	 * @param id
	 * @return Texture with id
	 */
	public Texture getTexture(String id) {
		return get(assets.get(id), Texture.class);
	}

	/**
	 * Get a TextureAtlas from the asset manager stored via the XML id value
	 * 
	 * @param id
	 * @return TextureAtlas with id
	 */
	public TextureAtlas getTextureAtlas(String id) {
		return get(assets.get(id), TextureAtlas.class);
	}

	/**
	 * Get a TiledMap from the asset manager stored via the XML id value
	 * 
	 * @param id
	 * @return TiledMap with id
	 */
	public TiledMap getTiledMap(String id) {
		return get(assets.get(id), TiledMap.class);
	}

	/**
	 * Check for an asset is loaded with the id stored via the XML id value
	 * 
	 * @param id
	 *            asset id
	 * @return asset at id is loaded
	 */
	private String checkForAsset(String id) {
		if (assets.containsKey(id))
			return assets.get(id);
		else
			return id;
	}

}
