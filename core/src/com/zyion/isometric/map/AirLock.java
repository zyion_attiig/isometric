package com.zyion.isometric.map;

import com.badlogic.gdx.utils.Array;

public class AirLock extends Door {

	private Array<Debris> debris = new Array<Debris>();
	
	public AirLock(TileMap map, int tileX, int tileY, int type) {
		super(map, tileX, tileY, type, true);
	}
	
	public void addDebris(Debris debree) {
		debris.add(debree);
	}

    @Override
	public void update() {
		super.update();
		if (!blocking) {
			for (Debris debree : debris) {
				debree.blocking = false;
				debree.moveTo(tileX, tileY);
			}
		}
	}

	@Override
    public void collision(MapObject object, boolean collision) {
         super.collision(object, collision);
         if (object instanceof Debris) {
        	 object.remove = true;
        	 debris.removeValue((Debris) object, false);
         }
    }

}
