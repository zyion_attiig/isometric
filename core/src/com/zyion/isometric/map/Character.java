package com.zyion.isometric.map;

import com.badlogic.gdx.utils.Array;

/**
 * Created by Zyion on 16/01/2016.
 */
public class Character extends MapObject {

    public Array<Item> items = new Array<Item>();
    public boolean shot = false;

    public Character(TileMap map, int tileX, int tileY, int type) {
        super(map, tileX, tileY, type, true);
    }
    
    public int frame = 0;
    private int totalFrames = 4;
    private int ticks = 0;
    private int totalTicks = 10;

    @Override
	public void update() {
		super.update();
		
		if (moving) {
			if (ticks > totalTicks) {
				ticks = 0;
				frame++;
				if (frame >= totalFrames) {
					frame = 0;
				}
			} else ticks++;
		} else {
			frame = 0;
			ticks = 0;
		}
	}



	public boolean hasItem(String id) {
    	for (Item item : items) {
    		if (item.id.equalsIgnoreCase(id)) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public void removeItem(String id) {
    	for (int i = 0; i < items.size; i++) {
    		if (items.get(i).id.equalsIgnoreCase(id)) {
    			items.removeIndex(i);
    			break;
    		}
    	}
    }

    public void addItem(Item item) {
        items.add(item);
    }
    
}
