package com.zyion.isometric.map;

/**
 * Created by Zyion on 9/01/2016.
 */
public class Debris extends MapObject {

    public Debris(TileMap map, int tileX, int tileY, int type) {
        super(map, tileX, tileY, type, true);
        velocity = 1;
    }

}
