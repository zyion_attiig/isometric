package com.zyion.isometric.map;

/**
 * Created by Zyion on 6/01/2016.
 */
public class Door extends MapObject {

    public int state = 0;
    final int finalState = 3;
    boolean opening = false, closing = false, collision = false;
    int coolDown = 0;
    boolean clickable = true;

    public Door(TileMap map, int tileX, int tileY, int type, boolean clickable) {
        super(map, tileX, tileY, type);
        this.clickable = clickable;
    }

    public void update() {
        super.update();

        if (opening) {
            if (state == finalState) {
                opening = false;
                coolDown = 20;
            } else {
                state++;
            }
        }

        if (closing) {
            if (state == 0) {
                closing = false;
                coolDown = 20;
            } else {
                state--;
            }
        }

        switch (state) {
            case finalState:
                blocking = false;
                break;
            default:
                blocking = true;
                break;
        }

        if (coolDown > 0) coolDown--;
    }

    public void open() {
        opening = true;
    }

    public int isOpen() {
        return state;
    }

    public void close() {
        closing = true;
    }

    public void toggle() {
        blocking = true;
        if (!opening && !closing && coolDown == 0) {
            if (state == finalState && !collision) {
                closing = true;
            } else if (state == 0) {
                opening = true;
            }
        }
    }


    @Override
    public void collision(MapObject object, boolean collision) {
        this.collision = collision;
    }

    @Override
    public void clicked(MapObject object) {
        if (clickable) {
            int tileDistX = tileX - object.tileX;
            int tileDistY = tileY - object.tileY;
            int distance = Math.abs(tileDistX) + Math.abs(tileDistY);
            if (distance == clickDistance) {
                toggle();
            }
        }
    }

}
