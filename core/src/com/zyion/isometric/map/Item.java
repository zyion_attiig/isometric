package com.zyion.isometric.map;

/**
 * Created by Zyion on 16/01/2016.
 */
public class Item extends MapObject {

	public String id;
	
    public Item(TileMap map, int tileX, int tileY, int type, String id) {
        super(map, tileX, tileY, type);
        this.id = id;
    }

    @Override
    public void collision(MapObject object, boolean collision) {
        if (object instanceof Character) {  
        	remove = true;
        	((Character)object).addItem(this);
        	
        }
    }

}
