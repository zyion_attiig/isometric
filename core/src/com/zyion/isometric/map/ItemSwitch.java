package com.zyion.isometric.map;

/**
 * Created by Zyion on 16/01/2016.
 */
public class ItemSwitch extends Switch {

	private boolean powered = false;
	
    public ItemSwitch(TileMap map, int tileX, int tileY, int type, Door door, boolean blocking, int clickRange, boolean clickable) {
        super(map, tileX, tileY, type, door, blocking, clickRange, clickable);
    }

    @Override
    public void clicked(MapObject object) {
        int tileDistX = tileX - object.tileX;
        int tileDistY = tileY - object.tileY;
        int distance = Math.abs(tileDistX) + Math.abs(tileDistY);
        if (distance == clickDistance && clickable) {
            if (object instanceof Character) {
                Character character = (Character) object;

                // remove items if has both batteries
                if (character.hasItem("battery_1") && character.hasItem("battery_2")) {
                    character.items.clear();
                    powered = true;
                }
                
            }
            if (powered) {
                for (MapObject obj : objects) {
                    if (obj instanceof Door) {
                        ((Door)obj).toggle();
                    } else if (obj instanceof StoryPoint) {
                        obj.collision(object, true);
                    } else if (obj instanceof StoryPoint) {
                    	((Turret)obj).toggle();
                    }
                }
            }
        }
    }

}
