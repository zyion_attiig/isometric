package com.zyion.isometric.map;

import com.badlogic.gdx.math.Vector2;

import java.util.LinkedList;

/**
 * Created by Zyion on 27/12/2015.
 */
public class MapObject {

    public TileMap map;
    public int tileX, tileY, type, goalX, goalY;
    public Vector2 position, destination;
    public int clickDistance = 1;
    public boolean remove = false;
    public boolean blocking = false;
    public int velocity = 2;
    public double angle = 0;
    protected boolean moving = false;

    private LinkedList<PathFinder.MapCell> path = new LinkedList<PathFinder.MapCell>();

    public MapObject(TileMap map, int tileX, int tileY, int type) {
        this.map = map;
        this.type = type;
        setTo(tileX, tileY);
    }

    public MapObject(TileMap map, int tileX, int tileY, int type, boolean blocking) {
        this.map = map;
        this.type = type;
        setTo(tileX, tileY);
        this.blocking = blocking;
    }

    public void collision(MapObject object, boolean collision) {

    }

    public void clicked(MapObject object) {

    }

    public void update() {
    	moving = false;
        // move to destination tile
        if (getDistance(destination, position) > 1) {
            angle = getAngleRads(destination, position);
            position.x += Math.cos(angle) * velocity;
            position.y += Math.sin(angle) * velocity;
            moving = true;
        } else if (path.size() > 0) {
            PathFinder.MapCell cell = path.removeFirst();
            destination = map.getTileCenterPosition(cell.x, cell.y);
            moving = true;
        }
        // check tile position
        Vector2 currentTile = map.getTileIndexFromPosition(position.x, position.y);
        if (currentTile.x != tileX || currentTile.y != tileY) {
            // remove from current cell
            if (map.containsTile(tileX, tileY)) {
                map.getCellAt(tileX, tileY).removeObject(this);
            }
            // update current cell
            this.tileX = (int) currentTile.x;
            this.tileY = (int) currentTile.y;
            map.getCellAt(tileX, tileY).addObject(this);
            path = PathFinder.find(tileX, tileY, goalX, goalY, map);
            moving = true;
        }
    }

    public void setTo(int tx, int ty) {
        // check destination cell
        if (map.containsTile(tx, ty)) {
            path.clear();
            // current cell
            if (map.containsTile(tileX, tileY)) {
                map.getCellAt(tileX, tileY).removeObject(this);
            }
            // set destination and position
            this.destination = map.getTileCenterPosition(tx, ty);
            this.position = destination;
            this.tileX = tx;
            this.tileY = ty;
            // add to map
            map.getCellAt(tx, ty).addObject(this);
        }
    }

    public void moveTo(int tx, int ty) {
        path = PathFinder.find(tileX, tileY, tx, ty, map);
        this.goalX = tx;
        this.goalY = ty;
    }



    public static float getAngleRads(Vector2 to, Vector2 from) {
        return (float) Math.atan2(to.y - from.y, to.x - from.x);
    }

    public static float getAngleDeg(Vector2 to, Vector2 from) {
        return (float) Math.toDegrees(Math.atan2(to.y - from.y, to.x - from.x)) - 90;
    }

    public static float getDistance(Vector2 to, Vector2 from) {
        return (float) Math.sqrt((to.x - from.x) * (to.x - from.x) + (to.y - from.y) * (to.y - from.y));
    }

}
