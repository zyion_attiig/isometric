package com.zyion.isometric.map;

import com.zyion.isometric.TextDisplay;

/**
 * Created by Zyion on 9/01/2016.
 */
public class MapObjectFactory {

    private boolean engine_finished = false;
    private boolean cockpit_finished = false;
    private boolean sarah_seen = false;

    public void loadMap(TileMap map, String mapName, TextDisplay textDisplay, MapObject playerObject) {
    	
    	//Turret turret = new Turret(map, 2, 3, 2, true);
    	//Switch switchy = new Switch(map, 3, 3, 2, true, 1, true);
    	//switchy.addObject(turret);
    	//map.addObject(turret);
    	//map.addObject(switchy);
    	
        // game maps
        if (mapName.equalsIgnoreCase("cryogenic")) {
            loadCyro(map,textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("safe_area")) {
            loadSafe(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("cryogenic_safety_blocker")) {
            loadCryoSafe(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("four_way_connection_room")) {
            loadFourWay(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("habitat_corridor")) {
            loadHabitat(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("escape_corridor")) {
            loadEscape(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("engine_maintenance_room")) {
            loadEngMaintenance(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("main_engine_room")) {
            loadMainEng(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("common_room")) {
            loadCommon(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("control_room")) {
            loadControl(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("cockpit")) {
            loadCockpit(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("left_cannon_room")) {
            loadLeftCannon(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("left_engine_room")) {
            loadLeftEng(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("right_cannon_room")) {
            loadRightCannon(map, textDisplay, playerObject);
        }

        if (mapName.equalsIgnoreCase("right_engine_room")) {
            loadRightEng(map, textDisplay, playerObject);
        }


        // test map
        if (mapName.equalsIgnoreCase("map")) {
            loadMapObjects(map);
        }

    }

    //Zyions default map.
    private void loadMapObjects(TileMap map) {

        //map.addObject(new MapSwitcher(map, 5, 4, 0, "map", 26, 4));

        map.addObject(new PushableObject(map, 14, 6, 1));
        map.addObject(new PushableObject(map, 19, 19, 1));
        map.addObject(new PushableObject(map, 7, 20, 1));
        map.addObject(new PushableObject(map, 6, 26, 1));
        map.addObject(new PushableObject(map, 24, 24, 1));
        map.addObject(new PushableObject(map, 8, 28, 1));
        map.addObject(new PushableObject(map, 20, 20, 1));
        map.addObject(new PushableObject(map, 5, 5, 1));

        map.addObject(new Door(map, 8, 15, 0, true));
        map.addObject(new Door(map, 13, 10, 0, true));
        map.addObject(new Door(map, 23, 15, 0, true));
        map.addObject(new Door(map, 16, 23, 1, true));
        map.addObject(new Door(map, 8, 5, 1, true));

        Door door = new Door(map, 3, 3, 0, false);
        map.addObject(door);
        map.addObject(new PressurePlate(map, 2, 5, 1, door));

    }

    //First Cryogenic pod room.
    private void loadCyro(TileMap map, TextDisplay textDisplay, MapObject playerObject){
        /*AirLock air = new AirLock(map, 1, 8, 0);
        for (int i = 1; i < 4; i++) {
            Debris debree = new Debris(map, i, 6, 0);
            map.addObject(debree);
            air.addDebris(debree);
        }
        map.addObject(air);*/



        //map switchers:
        map.addObject(new MapSwitcher(map, 4, 11, 0, "safe_area", 4, 1, this));

        map.addObject(new StoryPoint(map, 4, 4, 0, textDisplay, new String[]{"Ben: Ugh..... Where am I...",
                "Elias: I have woken you up Ben.",
                "Ben: Oh, Hi Elias...",
                "Elias: There was a power outage,\n and the captain went to fix it.",
                "Elias: However the captain never came back\n after getting to the cockpit",
                "Elias: James and Sarah went too but no response...",
                "Ben: So it's my turn to fix it. Ok"}, playerObject, 4, 4, true));

        //map.addObject(new StoryPoint(map, 4, 8, 0, textDisplay, new String[]{"Not as easy as you thought ", "Was it? "}, playerObject, 2, 8));
        //map.addObject(new StoryPoint(map, 2, 8, 0, textDisplay, new String[]{"YOU ARE A ", " @#$! $!#$! $!#$ !#$! "}, playerObject, 2, 2));
        Door openDoor1 = new Door(map, 4, 11, 0, false);
        if (openDoor1.isOpen() == 0)
            openDoor1.toggle();
        map.addObject(openDoor1);

        Door cryo1 = new Door(map, 1, 3, 4, false);
        map.addObject(cryo1);
        Door cryo2 = new Door(map, 1, 5, 4, false);
        map.addObject(cryo2);
        Door cryo3 = new Door(map, 1, 7, 4, false);
        map.addObject(cryo3);
        Door cryo4 = new Door(map, 1, 9, 4, false);
        map.addObject(cryo4);

    }

    //Cryo Safe Room *Tute 1 room*
    private void loadSafe(TileMap map, TextDisplay textDisplay, MapObject playerObject) {
        map.addObject(new StoryPoint(map, 4, 1, 0, textDisplay, new String[]{"Elias: Since the Ship is running on emergency power,", "Elias: there are buttons to open the doors.", "Elias: When you want to go to the next room.", "Elias: Go next to one of the buttons and look for\n the right button to open the door.", "Elias: Try it on this button here."}, playerObject, 4, 2));
        //map switchers:
        map.addObject(new MapSwitcher(map, 4, 15, 0, "cryogenic_safety_blocker", 4, 1, this));
        map.addObject(new MapSwitcher(map, 4, 0, 0, "cryogenic", 4, 10, this));

        //Other objects:
        Door doorToSafety = new Door(map, 4, 15, 0, false);
        map.addObject(doorToSafety);
        map.addObject(new Switch(map, 4, 7, 0, doorToSafety, true, 1, true));
        Door openDoor2 = new Door(map, 4, 0, 0, false);
        if (openDoor2.isOpen() == 0)
            openDoor2.toggle();
        map.addObject(openDoor2);
    }

    //Cryo Block Room *Tute 2 room*
    private void loadCryoSafe(TileMap map, TextDisplay textDisplay, MapObject playerObject) {
        map.addObject(new StoryPoint(map, 4, 1, 0, textDisplay, new String[]{"Elias: Ben, the emergency mode must be activated.", "Elias: Some hallways have safety hatches.", "Elias: To open them push objects onto \n the pressure plates."}, playerObject, 4, 2));


        //map switchers:
        map.addObject(new MapSwitcher(map, 4, 15, 0, "four_way_connection_room", 16, 6, this));
        map.addObject(new MapSwitcher(map, 4, 0, 0, "safe_area", 4, 14, this));

        //Other objects:
        PressurePlate plate = new PressurePlate(map, 2, 6, 0);
            for (int i = 1; i <= 6; i++) {
                Door door = new Door(map, i, 8, 2, false);
                map.addObject(door);
                plate.addDoor(door);
            }

        map.addObject(new PushableObject(map, 4, 4, 1));

            Door openDoor2 = new Door(map, 4, 0, 0, false);
            if (openDoor2.isOpen() == 0)
                openDoor2.toggle();
            map.addObject(openDoor2);

        Door doorToFourWay = new Door(map, 4, 15, 0, false);
        map.addObject(doorToFourWay);
        map.addObject(new Switch(map, 2, 13, 0, doorToFourWay, true, 1, true));
    }

    //Four Way room to Habitat, Engine Maintenance & Escape pod
    private void loadFourWay(TileMap map, TextDisplay textDisplay, MapObject playerObject) {
        //map switchers:
        map.addObject(new MapSwitcher(map, 16, 23, 0, "engine_maintenance_room", 15, 1, this));
        map.addObject(new MapSwitcher(map, 16, 5, 0, "cryogenic_safety_blocker", 4, 14, this));
        map.addObject(new MapSwitcher(map, 5, 0, 0, "habitat_corridor", 12, 1, this));
        map.addObject(new MapSwitcher(map, 26, 0, 0, "escape_corridor", 3, 1, this));

        Door doorToMainEngine = new Door(map, 16, 23, 0, false);
        map.addObject(doorToMainEngine);
        map.addObject(new Switch(map, 6, 22, 0, doorToMainEngine, true, 1, true));

        Door doorToCryo = new Door(map, 16, 5, 0, false);
        map.addObject(doorToCryo);
        map.addObject(new Switch(map, 18, 9, 0, doorToCryo, true, 1, true));

        Door doorTohabitat = new Door(map, 5, 0, 0, false);
        map.addObject(doorTohabitat);
        //if (!engine_finished)
        //map.addObject(new Switch(map, 4, 5, 0, doorTohabitat, true, 1, false));
        //else
        map.addObject(new Switch(map, 4, 5, 0, doorTohabitat, true, 1, true));

        Door doorToHatch = new Door(map, 26, 0, 0, false);
        map.addObject(doorToHatch);
        if (!cockpit_finished)
            map.addObject(new Switch(map, 27, 5, 0, doorToHatch, true, 1, false));
        else
            map.addObject(new Switch(map, 27, 5, 0, doorToHatch, true, 1, true));

        //Other objects:
        PressurePlate plate1 = new PressurePlate(map, 4, 16, 0);
        PressurePlate plate2 = new PressurePlate(map, 6, 16, 0);
        PressurePlate plate3 = new PressurePlate(map, 8, 16, 0);
        for (int i = 22; i > 17; i--){
            Door door = new Door(map, 2, i, 3, false);
            map.addObject(door);
            plate1.addDoor(door);
        }

        for (int i = 22; i > 17; i--) {
            Door door = new Door(map, 3, i, 3, false);
            map.addObject(door);
            if (i > 18) {
                plate2.addDoor(door);
                plate1.addDoor(door);
            } else
                plate1.addDoor(door);

        }

        for (int i = 22; i > 17; i--) {
            Door door = new Door(map, 4, i, 3, false);
            map.addObject(door);
            if (i == 19) {
                plate2.addDoor(door);
                plate1.addDoor(door);
            } else if ( i == 18)
                plate1.addDoor(door);

        }

        for (int i = 20; i > 17; i--) {
            Door door = new Door(map, 5, i, 3, false);
            map.addObject(door);
            if (i == 19) {
                plate2.addDoor(door);
                plate1.addDoor(door);
            } else if ( i == 18)
                plate1.addDoor(door);

        }

        for (int i = 20; i > 17; i--) {
            Door door = new Door(map, 6, i, 3, false);
            map.addObject(door);
            if (i == 20)
                plate3.addDoor(door);
            if (i == 19) {
                plate2.addDoor(door);
            } else if ( i == 18) {
                plate1.addDoor(door);
                plate3.addDoor(door);
            }

        }

        for (int i = 20; i > 17; i--) {
            Door door = new Door(map, 7, i, 3, false);
            map.addObject(door);
            if (i == 19) {
                plate2.addDoor(door);
                plate1.addDoor(door);
            } else if ( i == 18)
                plate1.addDoor(door);

        }

        for (int i = 22; i > 17; i--) {
            Door door = new Door(map, 8, i, 3, false);
            map.addObject(door);
            if (i == 19) {
                plate2.addDoor(door);
                plate1.addDoor(door);
            } else if ( i == 18)
                plate1.addDoor(door);

        }

        for (int i = 22; i > 17; i--) {
            Door door = new Door(map, 9, i, 3, false);
            map.addObject(door);
            if (i > 18) {
                plate2.addDoor(door);
                plate1.addDoor(door);
            } else
                plate1.addDoor(door);

        }

        for (int i = 22; i > 17; i--){
            Door door = new Door(map, 10, i, 3, false);
            map.addObject(door);
            plate1.addDoor(door);
        }
        map.addObject(new PushableObject(map, 6, 12, 1));
        map.addObject(new PushableObject(map, 16, 12, 1));
    }

    //Habitat Corridor
    private void loadHabitat(TileMap map, TextDisplay textDisplay, MapObject playerObject) {

        map.addObject(new StoryPoint(map, 12, 1, 0, textDisplay, new String[]{"Elias: Looks like there is Debri in this hallway.", "Elias: There is an airlock switch in the room next to you", "Elias: Open the airlock and the debri might be pulled"}, playerObject, 12, 2));
        //map switchers
        map.addObject(new MapSwitcher(map, 12, 0, 0, "four_way_connection_room", 5, 1, this));
        map.addObject(new MapSwitcher(map, 11, 31, 0, "common_room", 26, 1, this));

        //Other objects:

        AirLock air = new AirLock(map, 15, 15, 1);
        for (int i = 7; i < 15; i++) {
            Debris debree = new Debris(map, i, 15, 0);
            map.addObject(debree);
            air.addDebris(debree);
        }
        map.addObject(air);

        map.addObject(new Switch(map, 3, 7, 0, air, true, 1, true));

        Door doorToFourway = new Door(map, 12, 0, 0, false);
        map.addObject(doorToFourway);
        map.addObject(new Switch(map, 13, 2, 0, doorToFourway, true, 1, true));

        Door doorToVacuum = new Door(map, 8, 2, 1, false);
        map.addObject(doorToVacuum);
        map.addObject(new Switch(map, 9, 4, 0, doorToVacuum, true, 1, true));
        map.addObject(new Switch(map, 1, 4, 0, doorToVacuum, true, 1, true));

        Door doorToCommon = new Door(map, 11, 31, 0, false);
        map.addObject(doorToCommon);
        map.addObject(new Switch(map, 9, 30, 0, doorToCommon, true, 1, true));

        //put vacuum here
    }

    //Escape pod Corridor
    private void loadEscape(TileMap map, TextDisplay textDisplay, MapObject playerObject) {
        map.addObject(new StoryPoint(map, 4, 30, 0, textDisplay, new String[]{"Ben: Oh right, in emergencies the escape pod \n needs batteries", "Ben: Each side engine should have spare batteries."}, playerObject, 4, 29));

        //map switchers:
        map.addObject(new MapSwitcher(map, 3, 0, 0, "four_way_connection_room", 26, 1, this));
        map.addObject(new MapSwitcher(map, 4, 31, 0, "common_room", 5, 1, this));

        //Other objects:
        Door doorToCommon = new Door(map, 4, 31, 0, false);
        map.addObject(doorToCommon);
        map.addObject(new Switch(map, 2, 30, 0, doorToCommon, true, 1, true));

        Door doorToFourway = new Door(map, 3, 0, 0, false);
        map.addObject(doorToFourway);
        map.addObject(new Switch(map, 2, 3, 0, doorToFourway, true, 1, true));

        Door doorToHatch = new Door(map, 7, 2, 1, false);
        map.addObject(doorToHatch);
        map.addObject(new Switch(map, 4, 6, 0, doorToHatch, true, 1, true));

        Door door = new Door(map, 0, 16, 1, false);
        ItemSwitch itemSwitch = new ItemSwitch(map, 1, 17, 0, door, true, 1, true);

        map.addObject(door);
        map.addObject(itemSwitch);
    }

    //Engine Maintenance room.
    private void loadEngMaintenance(TileMap map, TextDisplay textDisplay, MapObject playerObject){
        //map switchers:
        map.addObject(new MapSwitcher(map, 15, 0, 0, "four_way_connection_room", 16, 22, this));
        map.addObject(new MapSwitcher(map, 15, 17, 0, "main_engine_room", 13, 1, this));
        map.addObject(new MapSwitcher(map, 31, 8, 0, "left_engine_room", 1, 19, this));
        map.addObject(new MapSwitcher(map, 0, 8, 0, "right_engine_room", 24, 19, this));

        //Other objects:
        // this is for the computers when they are ready.
        for (int i = 11; i < 20; i++){
            Door door = new Door(map, i, 8, 10, false);
            map.addObject(door);
        }

        for (int i = 11; i < 20; i++){
            Door door = new Door(map, i, 11, 10, false);
            map.addObject(door);
        }

        //doors:

        Door doorToLeftEng = new Door(map, 31, 8, 1, false);
        map.addObject(doorToLeftEng);
        if (!cockpit_finished)
            map.addObject(new Switch(map, 28, 9, 0, doorToLeftEng, true, 1, false));
        else
            map.addObject(new Switch(map, 28, 9, 0, doorToLeftEng, true, 1, true));

        Door doorToRightEng = new Door(map, 0, 8, 1, false);
        map.addObject(doorToRightEng);
        //if (!cockpit_finished)
            //map.addObject(new Switch(map, 3, 9, 0, doorToRightEng, true, 1, false));
        //else
            map.addObject(new Switch(map, 3, 9, 0, doorToRightEng, true, 1, true));

        Door doorToFourway = new Door(map, 15, 0, 0, false);
        map.addObject(doorToFourway);
        map.addObject(new Switch(map, 16, 3, 0, doorToFourway, true, 1, true));

        Door doorToMainEng = new Door(map, 15, 17, 0, false);
        map.addObject(doorToMainEng);
        //if (engine_finished) {
           // map.addObject(new Switch(map, 18, 15, 0, doorToMainEng, true, 1, false));
        //}
        //else
            map.addObject(new Switch(map, 18, 15, 0, doorToMainEng, true, 1, true));
    }

    //Main Engine Power Room.
    private void loadMainEng(TileMap map, TextDisplay textDisplay, MapObject playerObject) {
        /*
        Is there a way to disable the click onto the panel?
        */
        map.addObject(new StoryPoint(map, 13, 1, 0, textDisplay, new String[]{"Ben: Awesome, let's turn on the power."}, playerObject, 2, 8));
        map.addObject(new StoryPoint(map, 2, 8, 0, textDisplay, new String[]{"Ben: Power is ba.... Jack???"}, playerObject, 2, 12));
        map.addObject(new StoryPoint(map, 2, 12, 0, textDisplay, new String[]{"Ben: Elias, what happened?", "Elias: I don't know, his coms went offline \n when we got out of the cryo pod.", "Ben: Looks like Jack left a message...", "Jack: Every...ne.... *static* Don't trus.......", "Ben: WHO! ... Let's go find the Cap quick.", "Elias: He should be in the cockpit."}, playerObject, 3, 12));


        //map switchers:
        map.addObject(new MapSwitcher(map, 13, 0, 0, "engine_maintenance_room", 15, 16, this));
        engine_finished = true;


        //Other objects:
        Door doorToMaintenance = new Door(map, 13, 0, 0, false);
        map.addObject(doorToMaintenance);
        map.addObject(new Switch(map, 16, 3, 0, doorToMaintenance, true, 1, true));

        Door Power = new Door(map, 1, 8, 6, false);
        map.addObject(Power);

        for (int i = 14; i > 10; i--) {
            Door Storage1 = new Door(map, 4, i, 8, false);
            map.addObject(Storage1);
        }

        Door storage2 = new Door(map, 3, 11, 8, false);
        Door storage3 = new Door(map, 1, 13, 8, false);
        map.addObject(storage2);
        map.addObject(storage3);

        Door jack = new Door(map, 2, 13, 7, false);
        map.addObject(jack);
    }

    //Common Area.
    private void loadCommon(TileMap map, TextDisplay textDisplay, MapObject playerObject){
        //map switchers:
        map.addObject(new MapSwitcher(map, 26, 0, 0, "habitat_corridor", 11, 30, this));
        map.addObject(new MapSwitcher(map, 5, 0, 0, "escape_corridor", 4, 30, this));
        map.addObject(new MapSwitcher(map, 15, 31, 0, "control_room", 10, 1, this));
        map.addObject(new MapSwitcher(map, 31, 10, 0, "right_cannon_room", 1, 3, this));
        map.addObject(new MapSwitcher(map, 0, 10, 0, "left_cannon_room", 8, 3, this));

        //Other objects:
        Door doorToHabitat = new Door(map, 26, 0, 0, false);
        map.addObject(doorToHabitat);
        map.addObject(new Switch(map, 27, 3, 0, doorToHabitat, true, 1, true));

        Door doorToEscape = new Door(map, 5, 0, 0, false);
        map.addObject(doorToEscape);
        if (sarah_seen)
            map.addObject(new Switch(map, 4, 3, 0, doorToEscape, true, 1, true));
        else
            map.addObject(new Switch(map, 4, 3, 0, doorToEscape, true, 1, false));

        Door doorToControl = new Door(map, 15, 31, 0, false);
        map.addObject(doorToControl);
        map.addObject(new Switch(map, 22, 30, 0, doorToControl, true, 1, true));

        //maze for control:
        PressurePlate plate1 = new PressurePlate(map, 23, 11, 0);
        PressurePlate plate2 = new PressurePlate(map, 22, 15, 0);
        for (int i = 9; i < 14; i++){
            Door door = new Door(map, 21, i, 3, false);
            map.addObject(door);
            plate1.addDoor(door);
            plate2.addDoor(door);
        }
        for (int i = 22; i < 31; i++){
            Door door = new Door(map, i, 13, 3, false);
            map.addObject(door);
            plate1.addDoor(door);
            plate2.addDoor(door);
        }

        map.addObject(new PushableObject(map, 23, 4, 1));
        map.addObject(new PushableObject(map, 12, 15, 1));

        Door doorToRightCannon = new Door(map, 31, 10, 1, false);
        map.addObject(doorToRightCannon);
        if (cockpit_finished)
            map.addObject(new Switch(map, 28, 11, 0, doorToRightCannon, true, 1, true));
        else
            map.addObject(new Switch(map, 28, 11, 0, doorToRightCannon, true, 1, false));

        Door doorToLeftCannon = new Door(map, 0, 10, 1, false);
        map.addObject(doorToLeftCannon);
        if(cockpit_finished)
            map.addObject(new Switch(map, 1, 11, 0, doorToLeftCannon, true, 1, true));
        else
            map.addObject(new Switch(map, 1, 11, 0, doorToLeftCannon, true, 1, false));
    }

    //Control Office
    private void loadControl(TileMap map, TextDisplay textDisplay, MapObject playerObject){
        //map switchers:
        map.addObject(new MapSwitcher(map, 10, 0, 0, "common_room", 15, 30, this));
        map.addObject(new MapSwitcher(map, 10, 31, 0, "cockpit", 3, 1, this));

        //Other objects:
        Door doorToCommon = new Door(map, 10, 0, 0, false);
        map.addObject(doorToCommon);
        map.addObject(new Switch(map, 11, 3, 0, doorToCommon, true, 1, true));

        Door doorToCockpit = new Door(map, 10, 31, 0, false);
        map.addObject(doorToCockpit);
        if (!cockpit_finished)
            map.addObject(new Switch(map, 8, 30, 0, doorToCockpit, true, 1, true));
        else
            map.addObject(new Switch(map, 8, 30, 0, doorToCockpit, true, 1, false));

        Switch plate1 = new Switch(map, 8, 6, 0, true, 1, true);
        Switch plate2 = new Switch(map, 8, 9, 0, true, 1, true);
        //PressurePlate plate2 = new PressurePlate(map, 22, 15, 0);
        for (int i = 1; i < 9; i++){
            Door door = new Door(map, i, 7, 8, false);
            map.addObject(door);
        }
        for (int i = 9; i < 11; i++){
            Door door = new Door(map, i, 7, 2, false);
            map.addObject(door);
            plate1.addObject(door);
            plate2.addObject(door);
        }

        for (int i = 11; i < 19; i++){
            Door door = new Door(map, i, 7, 8, false);
            map.addObject(door);
        }


        for (int i = 12; i < 26; i++){
            Door door = new Door(map, 9, i, 11, false);
            map.addObject(door);
        }

        if (cockpit_finished){
            map.addObject(new StoryPoint(map, 10, 30, 0, textDisplay, new String[]{"Ben: Why is the security activated...", "Ben: There has to be a switch... \n on the other side of the laser...", "Ben: Great... Let's find another way. \n can I move this block?"}, playerObject, 10, 29));

            Turret turret = new Turret(map, 18, 22, 2, true);
            Switch switchy = new Switch(map, 17, 21, 2, true, 1, true);
            switchy.addObject(turret);
            map.addObject(turret);
            map.addObject(switchy);

            Turret turret2 = new Turret(map, 1, 18, 0, true);
            map.addObject(turret2);

            map.addObject(new PushableObject(map, 10, 28, 1));
        }

    }

    //The "Cock" pit
    private void loadCockpit(TileMap map, TextDisplay textDisplay, MapObject playerObject) {
        map.addObject(new StoryPoint(map, 3, 1, 0, textDisplay, new String[]{"Ben: Captain, status report!", "Ben: Captain...?", "Ben: ....He left a message too...", "Captain: I left Sarah... In the ...... \n Cannon Room... She is safe....", "Elias: ......"}, playerObject, 2, 5));

        cockpit_finished = true;
        //map switchers:
        map.addObject(new MapSwitcher(map, 3, 0, 0, "control_room", 10, 30, this));

        //Other objects:
        Door openDoor1 = new Door(map, 3, 0, 0, false);
        if (openDoor1.isOpen() == 0)
            openDoor1.toggle();
        map.addObject(openDoor1);

        Door captain = new Door(map, 3, 4, 9, false);
        map.addObject(captain);
    }

    //Left Cannon Room
    private void loadLeftCannon(TileMap map, TextDisplay textDisplay, MapObject playerObject) {
        //map switchers:
        map.addObject(new MapSwitcher(map, 9, 3, 0, "common_room", 1, 10, this));

        //Other objects:

        Door doorToCommon = new Door(map, 9, 3, 1, false);
        map.addObject(doorToCommon);
        map.addObject(new Switch(map, 6, 7, 0, doorToCommon, true, 1, true));
    }

    //Left Engine room.
    private void loadLeftEng(TileMap map, TextDisplay textDisplay, MapObject playerObject){
        //map switchers:
        map.addObject(new MapSwitcher(map, 0, 19, 0, "engine_maintenance_room", 30, 8, this));

        //Other objects:

        Door doorToMaintenance = new Door(map, 0, 19, 1, false);
        map.addObject(doorToMaintenance);
        map.addObject(new Switch(map, 15, 6, 0, doorToMaintenance, true, 1, true));
        if (!((Character)playerObject).hasItem("battery_1"))
            map.addObject(new Item(map, 10, 5, 0, "battery_1"));
    }

    //Right Cannon Room
    private void loadRightCannon(TileMap map, TextDisplay textDisplay, MapObject playerObject){
        //map switchers:
        map.addObject(new MapSwitcher(map, 0, 3, 0, "common_room", 30, 10, this));
        map.addObject(new StoryPoint(map, 1, 3, 0, textDisplay, new String[]{"Ben: Sarah isn't here... Hmm what's that?", "Ben: It's Sarah's com equipment.", "Sarah: Ben! Get out of there! Elias! It's Elias!", "Sarah: Elias' AI system evolved, and is stopping \n humans from getting off Earth.", "Ben: Where was the escape pod again...", "Elias: Ben, you must stop."}, playerObject, 4, 4));


        Door doorToCommon = new Door(map, 0, 3, 1, false);
        map.addObject(doorToCommon);
        map.addObject(new Switch(map, 3, 6, 0, doorToCommon, true, 1, true));

        Door coms = new Door(map, 5, 4, 12, false);
        map.addObject(coms);

        //Other objects:
        sarah_seen = true;
    }

    //Right Engine Room.
    private void loadRightEng(TileMap map, TextDisplay textDisplay, MapObject playerObject){
        //map switchers
        map.addObject(new MapSwitcher(map, 25, 19, 0, "engine_maintenance_room", 1, 8, this));


        //Other objects:
        Door doorToMaintenance = new Door(map, 25, 19, 1, false);
        map.addObject(doorToMaintenance);
        map.addObject(new Switch(map, 15, 6, 0, doorToMaintenance, true, 1, true));
        if (!((Character)playerObject).hasItem("battery_2"))
            map.addObject(new Item(map, 3, 6, 0, "battery_2"));
    }
}