package com.zyion.isometric.map;

/**
 * Created by Zyion on 9/01/2016.
 */
public class MapSwitcher extends MapObject {

    String mapName;
    int toX, toY;
    MapObjectFactory factory;

    public MapSwitcher(TileMap map, int tileX, int tileY, int type, String mapName, int toX, int toY, MapObjectFactory factory) {
        super(map, tileX, tileY, type);
        this.mapName = mapName;
        this.toX = toX;
        this.toY = toY;
        this.factory = factory;
    }
}
