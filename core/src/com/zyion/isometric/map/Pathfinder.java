package com.zyion.isometric.map;

import java.util.LinkedList;

/**
 * Created by Zyion on 6/01/2016.
 */
public class PathFinder {

    public static class MapCell {

        public int x, y;
        public MapCell from;

        public MapCell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public boolean equals(MapCell cell) {
            if (x == cell.x && y == cell.y) return true;
            else return false;
        }
    }

    public static LinkedList<MapCell> find(int startX, int startY, int goalX, int goalY, TileMap map) {
        LinkedList<MapCell> path = new LinkedList<MapCell>();
        MapCell goal = new MapCell(goalX, goalY);
        MapCell start = new MapCell(startX, startY);
        LinkedList<MapCell> closedSet = new LinkedList<MapCell>();
        LinkedList<MapCell> openSet = new LinkedList<MapCell>();
        openSet.push(start);

        if (getPath(closedSet, openSet, goal, map)) {
            MapCell pathcell = closedSet.getFirst();
            while (!pathcell.equals(start)) {
                path.addFirst(pathcell);
                pathcell = pathcell.from;
            }
        }
        return path;
    }

    private static LinkedList<MapCell> getJoiningCells(int tx, int ty, TileMap map) {
        LinkedList<MapCell> cells = new LinkedList<MapCell>();
        // top
        if (!map.isCellBlocked(tx, ty + 1)) {
            cells.push(new MapCell(tx, ty + 1));
        }
        // bottom
        if (!map.isCellBlocked(tx, ty - 1)) {
            cells.push(new MapCell(tx, ty - 1));
        }
        // left
        if (!map.isCellBlocked(tx - 1, ty)) {
            cells.push(new MapCell(tx - 1, ty));
        }
        // right
        if (!map.isCellBlocked(tx + 1, ty)) {
            cells.push(new MapCell(tx + 1, ty));
        }
        return cells;
    }

    private static boolean hasCell(LinkedList<MapCell> closedSet, MapCell cell) {
        for (MapCell closedCell : closedSet) {
            if (closedCell.equals(cell)) {
                return true;
            }
        }
        return false;
    }

    private static boolean getPath(LinkedList<MapCell> closedSet, LinkedList<MapCell> openSet, MapCell goal, TileMap map) {
        LinkedList<MapCell> list = new LinkedList<MapCell>();
        for (MapCell mapCell : openSet) {
            LinkedList<MapCell> frontier = getJoiningCells(mapCell.x, mapCell.y, map);
            for (MapCell cell : frontier) {
                cell.from = mapCell;
                if (cell.equals(goal)) {
                    closedSet.push(cell);
                    return true;
                } else {
                    if (!hasCell(closedSet, cell)) {
                        closedSet.push(cell);
                        list.add(cell);
                    }
                }
            }
        }
        if (list.size() == 0) return false;
        else return getPath(closedSet, list, goal, map);
    }
}
