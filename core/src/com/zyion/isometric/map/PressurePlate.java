package com.zyion.isometric.map;

import com.badlogic.gdx.utils.Array;

/**
 * Created by Zyion on 7/01/2016.
 */
public class PressurePlate extends MapObject {

    private Array<Door> doors = new Array<Door>();

    public PressurePlate(TileMap map, int tileX, int tileY, int type) {
        super(map, tileX, tileY, type);
    }

    public PressurePlate(TileMap map, int tileX, int tileY, int type, Door door) {
        super(map, tileX, tileY, type);
        doors.add(door);
    }

    public void addDoor(Door door) {
        doors.add(door);
    }

    @Override
    public void collision(MapObject object, boolean collision) {
        if (collision) {
            for (Door door : doors) {
                door.open();
            }
        } else {
            for (Door door : doors) {
                door.close();
            }
        }
    }

}
