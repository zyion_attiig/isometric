package com.zyion.isometric.map;

/**
 * Created by Zyion on 6/01/2016.
 */
public class PushableObject extends MapObject {

    public PushableObject(TileMap map, int tileX, int tileY, int type) {
        super(map, tileX, tileY, type, true);
    }

    @Override
    public void clicked(MapObject object) {
        int tileDistX = tileX - object.tileX;
        int tileDistY = tileY - object.tileY;
        int distance = Math.abs(tileDistX) + Math.abs(tileDistY);
        if (distance == clickDistance) {
            if (!map.isCellBlocked(tileX + tileDistX, tileY + tileDistY))
                moveTo(tileX + tileDistX, tileY + tileDistY);
        }
    }
}
