package com.zyion.isometric.map;

import com.zyion.isometric.TextDisplay;

/**
 * Created by Zyion on 11/01/2016.
 */
public class StoryPoint extends MapObject {

    private boolean expired = false;
    private TextDisplay textDisplay;
    private String[] conversation;
    private MapObject playerObject;
    private int playerMoveX, playerMoveY;

    public StoryPoint(TileMap map, int tileX, int tileY, int type, TextDisplay textDisplay, String[] conversation, MapObject playerObject, int playerMoveX, int playerMoveY) {
        super(map, tileX, tileY, type);
        this.textDisplay = textDisplay;
        this.conversation = conversation;
        this.playerObject = playerObject;
        this.playerMoveX = playerMoveX;
        this.playerMoveY = playerMoveY;
    }

    public StoryPoint(TileMap map, int tileX, int tileY, int type, TextDisplay textDisplay, String[] conversation, MapObject playerObject, int playerMoveX, int playerMoveY, boolean autoStart) {
        super(map, tileX, tileY, type);
        this.textDisplay = textDisplay;
        this.conversation = conversation;
        this.playerObject = playerObject;
        this.playerMoveX = playerMoveX;
        this.playerMoveY = playerMoveY;
        if (autoStart) {
            playerObject.moveTo(playerMoveX, playerMoveY);
            textDisplay.display(conversation);
            expired = true;
        }
    }

    @Override
    public void collision(MapObject object, boolean collision) {
        if (!expired) {
            playerObject.moveTo(playerMoveX, playerMoveY);
            textDisplay.display(conversation);
            expired = true;
        }
    }

}
