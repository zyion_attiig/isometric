package com.zyion.isometric.map;

import com.badlogic.gdx.utils.Array;

/**
 * Created by Zyion on 7/01/2016.
 */
public class Switch extends MapObject {
    protected boolean clickable;
    protected Array<MapObject> objects = new Array<MapObject>();

    public Switch(TileMap map, int tileX, int tileY, int type, boolean blocking, int clickRange, boolean clickable) {
        super(map, tileX, tileY, type);
        this.blocking = blocking;
        this.clickDistance = clickRange;
        this.clickable = clickable;
    }
    
    public Switch(TileMap map, int tileX, int tileY, int type, Door door) {
        super(map, tileX, tileY, type);
        clickDistance = 0;
        objects.add(door);
    }

    public Switch(TileMap map, int tileX, int tileY, int type, Door door, boolean blocking, int clickRange, boolean clickable) {
        super(map, tileX, tileY, type);
        this.blocking = blocking;
        this.clickDistance = clickRange;
        this.clickable = clickable;
        objects.add(door);
    }

    public void addObject(MapObject object) {
        objects.add(object);
    }

    @Override
    public void clicked(MapObject object) {
        int tileDistX = tileX - object.tileX;
        int tileDistY = tileY - object.tileY;
        int distance = Math.abs(tileDistX) + Math.abs(tileDistY);
        if (distance == clickDistance && clickable) {
            for (MapObject obj : objects) {
                if (obj instanceof Door) {
                    ((Door)obj).toggle();
                } else if (obj instanceof StoryPoint) {
                    obj.collision(object, true);
                } else if (obj instanceof Turret) {
                    ((Turret)obj).toggle();
                }

            }
        }
    }

}
