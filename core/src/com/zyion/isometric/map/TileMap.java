package com.zyion.isometric.map;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.zyion.isometric.ColorTools;
import com.zyion.isometric.InputHandler;
import com.zyion.isometric.TextDisplay;

/**
 * Created by Zyion on 26/12/2015.
 */
public class TileMap {

    public int width, height;
    public int size = 32;
    private Cell[][] cells;
    private int selectedTileX, selectedTileY;
    private MapObjectFactory mapObjectFactory;
    private Array<MapObject> objects = new Array<MapObject>();
    private HashMap<String, Pixmap> maps;
    private MapSwitcher switcher;
    private String mapName;

    public enum CellState {
        BLANK, HOVERED, CLICKED
    }

    public class Cell {

        public String hexColor;
        public CellState state = CellState.BLANK;
        private Array<MapObject> objects = new Array<MapObject>();

        public Cell(String hexColor) {
            this.hexColor = hexColor;
        }

        public Array<MapObject> getObjects() {
            return objects;
        }

        public void clicked(MapObject object) {
        	for (int i = 0; i < objects.size; i++) {
        		objects.get(i).clicked(object);
            }
        }

        public void addObject(MapObject object) {
            for (int i = 0; i < objects.size; i++) {
            	objects.get(i).collision(object, true);
            }
            objects.add(object);
        }

        public void removeObject(MapObject object) {
            objects.removeValue(object, false);
            for (int i = 0; i < objects.size; i++) {
            	objects.get(i).collision(object, false);
            }
        }
    }

    public void addObject(MapObject object) {
        objects.add(object);
    }

    public void removeObject(MapObject object) {
        objects.removeValue(object, false);
    }


    public TileMap(HashMap<String, Pixmap> pixmaps, MapObjectFactory mapObjectFactory) {
        this.maps = pixmaps;
        this.mapObjectFactory = mapObjectFactory;
    }


    public boolean loadMap(String mapName) {
        if (maps.containsKey(mapName)) {
        	this.mapName = mapName;
            Pixmap pixmap = maps.get(mapName);
            this.width = pixmap.getWidth();
            this.height = pixmap.getHeight();
            this.cells = new Cell[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    int pixel = pixmap.getPixel(i, height - j - 1);
                    String color = ColorTools.getColorString(pixel);
                    cells[i][j] = new Cell(color);
                }
            }
            Gdx.app.log("Map loaded", mapName + ", width: " + width + " height: " + height);
            return true;
        } else return false;
    }



    public void update() {
    	Array<MapObject> toRemove = new Array<MapObject>();
        for (MapObject object : objects) {
        	if (object.remove) {
        		toRemove.add(object);
        	} else {
        		object.update();
        	}
        }
        for (MapObject object : toRemove) {
        	objects.removeValue(object, false);
        	int tx = object.tileX;
        	int ty = object.tileY;
        	if (containsTile(tx, ty)) {
        		getCellAt(tx, ty).removeObject(object);
        	}
        }
    }
    
    private void load(TextDisplay textDisplay, Character playerObject) {
    	if (switcher != null) {
	        loadMap(switcher.mapName);
	        switcher.factory.loadMap(this, switcher.mapName, textDisplay, playerObject);
	        playerObject.map = this;
	        playerObject.setTo(switcher.toX, switcher.toY);
	        addObject(playerObject);
	        selectedTileX = 0;
	        selectedTileY = 0;
    	} else {
	        loadMap(mapName);
            mapObjectFactory.loadMap(this, "cryogenic", textDisplay, playerObject);
	        playerObject.map = this;
	        playerObject.setTo(4, 4);
	        addObject(playerObject);
	        selectedTileX = 0;
	        selectedTileY = 0;
    	}
    }

    public void updateInput(InputHandler input, TextDisplay textDisplay, Character playerObject) {

        // check player cell position
        int ptx = playerObject.tileX;
        int pty = playerObject.tileY;
        
        if (playerObject.shot) {
        	objects.clear();
        	load(textDisplay, playerObject);
        	playerObject.shot = false;
        }
        else if (containsTile(ptx, pty)) {
            Cell cell = cells[ptx][pty];
            // change map
            for (MapObject object : cell.objects) {
                if (object instanceof MapSwitcher) {
                    switcher = (MapSwitcher) object;
                    objects.clear();
                    load(textDisplay, playerObject);
                }
            }
        }

        // convert input position to map
        Vector2 mouse = input.getMousePosition();
        Vector2 mouseIso = from(mouse.x, mouse.y);
        int selectedX = (int)(mouseIso.x / size);
        int selectedY = (int)(mouseIso.y / size);

        if (containsTile(selectedX, selectedY)) {
            // compare last selected tile
            if (selectedX == selectedTileX && selectedY == selectedTileY) {
                cellHovered(selectedTileX, selectedTileY);
            } else {
                cellExited(selectedTileX, selectedTileY);
                selectedTileX = selectedX;
                selectedTileY = selectedY;
                cellEntered(selectedTileX, selectedTileY);
            }

            if (input.isLeftMouseButtonDown() && !textDisplay.displaying) {
                cellClicked(selectedX, selectedY, playerObject);
            }

        }
        
    }



    // cell input

    public void cellEntered(int x, int y) {
        getCellAt(x, y).state = CellState.HOVERED;
    }

    public void cellHovered(int x, int y) {
        getCellAt(x, y).state = CellState.HOVERED;
    }

    public void cellExited(int x, int y) {
        getCellAt(x, y).state = CellState.BLANK;
    }

    public void cellClicked(int x, int y, Character playerObject) {
        Cell cell = getCellAt(x, y);
        cell.state = CellState.CLICKED;
        cell.clicked(playerObject);

        if (!isCellBlocked(x, y)) {
            playerObject.moveTo(x, y);
        }
    }

    public boolean isCellBlocked(int x, int y) {
        if (containsTile(x, y)) {
            Cell cell = getCellAt(x, y);
            if (cell.hexColor.equalsIgnoreCase("#ffffff")) {
                for (MapObject object : cell.objects) {
                    if (object.blocking) return true;
                }
                return false;
            }
        }
        return true;
    }



    public Cell getCellAt(int x, int y) {
        return cells[x][y];
    }

    public boolean containsTile(int x, int y) {
        return (x >= 0 && x < width && y >= 0 && y < height);
    }

    public static Vector2 to(float x, float y) {
        return new Vector2(x + y, (y - x) / 2);
    }

    public Vector2 getTilePosition(int tx, int ty) {
        return to(tx * size, ty * size);
    }

    public Vector2 getTileCenterPosition(int tx, int ty) {
        return to((tx + .5f) * size, (ty + .5f) * size);
    }

    public Vector2 getTileIndexFromPosition(float x, float y) {
        return new Vector2((int) Math.floor((x - y * 2) / 2 / size), (int) Math.floor((x + y * 2) / 2 / size));
    }

    public static Vector2 from(float x, float y) {
        return new Vector2((x - y * 2) / 2, (x + y * 2) / 2);
    }

    public Cell[][] getCells() {
        return cells;
    }

}
