package com.zyion.isometric.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.zyion.isometric.Spritesheet;
import com.zyion.isometric.XMLAssetManager;

/**
 * Created by Zyion on 9/01/2016.
 */
public class TileMapRenderer {

    Texture selectionMask;

    private Texture img, block;

    ObjectMap<String, TextureRegion> tileTextures = new ObjectMap<String, TextureRegion>();
    TextureRegion[] doorXTextures = new TextureRegion[4];
    TextureRegion[] doorYTextures = new TextureRegion[4];
    TextureRegion[] doorTextures = new TextureRegion[4];
    TextureRegion[] doorSpikes = new TextureRegion[4];
    TextureRegion[] switchTextures = new TextureRegion[3];
    Texture[] beamTextures = new Texture[2];
    TextureRegion[] doorCyro = new TextureRegion[1];
    TextureRegion[] doorPower = new TextureRegion[1];
    TextureRegion[] Jack = new TextureRegion[1];
    TextureRegion[] doorCargo = new TextureRegion[1];
    TextureRegion[] Captain = new TextureRegion[1];
    TextureRegion[] compX = new TextureRegion[1];
    TextureRegion[] compY = new TextureRegion[1];
    TextureRegion[] radio = new TextureRegion[1];
    

    TextureRegion[] playerTexturesRight = new TextureRegion[4];
    TextureRegion[] playerTexturesUp = new TextureRegion[4];
    TextureRegion[] playerTexturesDown = new TextureRegion[4];
    TextureRegion[] playerTexturesLeft = new TextureRegion[4];


    public TileMapRenderer(XMLAssetManager assets) {

        img = new Texture("badlogic.jpg");
        block = assets.getTexture("block");

        // player
        Spritesheet playerSpritesheet = new Spritesheet(assets.getTexture("player"), 32, 64);
        playerTexturesRight[0] = playerSpritesheet.getSprite(0, 0);
        playerTexturesRight[1] = playerSpritesheet.getSprite(1, 0);
        playerTexturesRight[2] = playerSpritesheet.getSprite(2, 0);
        playerTexturesRight[3] = playerSpritesheet.getSprite(3, 0);
        playerTexturesUp[0] = playerSpritesheet.getSprite(0, 1);
        playerTexturesUp[1] = playerSpritesheet.getSprite(1, 1);
        playerTexturesUp[2] = playerSpritesheet.getSprite(2, 1);
        playerTexturesUp[3] = playerSpritesheet.getSprite(3, 1);
        playerTexturesDown[0] = playerSpritesheet.getSprite(3, 2);
        playerTexturesDown[1] = playerSpritesheet.getSprite(2, 2);
        playerTexturesDown[2] = playerSpritesheet.getSprite(1, 2);
        playerTexturesDown[3] = playerSpritesheet.getSprite(0, 2);
        playerTexturesLeft[0] = playerSpritesheet.getSprite(3, 3);
        playerTexturesLeft[1] = playerSpritesheet.getSprite(2, 3);
        playerTexturesLeft[2] = playerSpritesheet.getSprite(1, 3);
        playerTexturesLeft[3] = playerSpritesheet.getSprite(0, 3);
        
        // Door textures
        Spritesheet doorsSpritesheet = new Spritesheet(assets.getTexture("doors_spritesheet"), 64, 128);
        // door
        doorTextures[0] = doorsSpritesheet.getSprite(4, 0);
        doorTextures[1] = doorsSpritesheet.getSprite(5, 0);
        doorTextures[2] = doorsSpritesheet.getSprite(6, 0);
        doorTextures[3] = doorsSpritesheet.getSprite(7, 0);
        // Y facing
        doorYTextures[0] = doorsSpritesheet.getSprite(0, 0);
        doorYTextures[1] = doorsSpritesheet.getSprite(1, 0);
        doorYTextures[2] = doorsSpritesheet.getSprite(2, 0);
        doorYTextures[3] = doorsSpritesheet.getSprite(3, 0);
        // X facing
        doorXTextures[0] = doorsSpritesheet.getSprite(0, 1);
        doorXTextures[1] = doorsSpritesheet.getSprite(1, 1);
        doorXTextures[2] = doorsSpritesheet.getSprite(2, 1);
        doorXTextures[3] = doorsSpritesheet.getSprite(3, 1);
        //Spikes
        doorSpikes[0] = doorsSpritesheet.getSprite(4, 1);
        doorSpikes[1] = doorsSpritesheet.getSprite(5, 1);
        doorSpikes[2] = doorsSpritesheet.getSprite(6, 1);
        doorSpikes[3] = doorsSpritesheet.getSprite(7, 1);
        //Cryo
        doorCyro[0] = doorsSpritesheet.getSprite(0,2);
        //Power Switch
        doorPower[0] = doorsSpritesheet.getSprite(1, 2);
        //Jack
        Jack[0] = doorsSpritesheet.getSprite(2,2);
        //cargo
        doorCargo[0] = doorsSpritesheet.getSprite(3,2);
        //Captain
        Captain[0] = doorsSpritesheet.getSprite(4, 2);
        compX[0] = doorsSpritesheet.getSprite(5, 2);
        compY[0] = doorsSpritesheet.getSprite(6, 2);
        //radio
        radio[0] = doorsSpritesheet.getSprite(7, 2);



        // switches
        Spritesheet switchesSpritesheet = new Spritesheet(assets.getTexture("switches_spritesheet"), 64, 128);
        switchTextures[0] = switchesSpritesheet.getSprite(1, 0);
        switchTextures[1] = switchesSpritesheet.getSprite(2, 0);
        switchTextures[2] = switchesSpritesheet.getSprite(3, 0);


        // beam
        beamTextures[0] = assets.getTexture("beam_inverse");
        beamTextures[1] = assets.getTexture("beam_mask");


        // map tiles
        Spritesheet tileSpritesheet = new Spritesheet(assets.getTexture("tile_spritesheet"), 64, 128);

        // floor
        tileTextures.put("#ffffff",tileSpritesheet.getSprite(0, 0));
        // walls
        tileTextures.put("#ffff00", tileSpritesheet.getSprite(1, 0));
        tileTextures.put("#00ff00", tileSpritesheet.getSprite(2, 0));
        tileTextures.put("#aaaaaa", tileSpritesheet.getSprite(2, 1));
        // corner walls
        tileTextures.put("#ff0000", tileSpritesheet.getSprite(3, 0));
        tileTextures.put("#0000ff", tileSpritesheet.getSprite(4, 0));
        tileTextures.put("#aaaa00", tileSpritesheet.getSprite(5, 0));
        tileTextures.put("#00aa00", tileSpritesheet.getSprite(6, 0));
        // joining walls
        tileTextures.put("#ffaaaa", tileSpritesheet.getSprite(3, 1));
        tileTextures.put("#00aaaa", tileSpritesheet.getSprite(4, 1));
        tileTextures.put("#aaff00", tileSpritesheet.getSprite(5, 1));
        tileTextures.put("#00ffaa", tileSpritesheet.getSprite(6, 1));



        // doors
        tileTextures.put("#00ffff", tileSpritesheet.getSprite(1, 3));
        tileTextures.put("#ffaa00", tileSpritesheet.getSprite(0, 3));
        // doors open
        tileTextures.put("#00fffa", tileSpritesheet.getSprite(3, 3));
        tileTextures.put("#ffaa0a", tileSpritesheet.getSprite(2, 3));
    }


    public void renderMap(SpriteBatch batch, TileMap map) {

        Array<MapObject> postMapRenderObjects = new Array<MapObject>();

        //int px = playerObject.tileX;
        //int py = playerObject.tileY;

        Color col = batch.getColor();

        int height = map.height;
        int width = map.width;
        TileMap.Cell[][] cells = map.getCells();

/*        // floor cells
        for (int j = height - 1; j >= 0; j--) {
            for (int i = 0; i < width; i++) {
                TileMap.Cell cell = cells[i][j];
                String color = cell.hexColor;
                if (color.equalsIgnoreCase("#ffffff")) {
                    Vector2 iso = map.getTilePosition(i, j);
                    switch (cell.state) {
                        case CLICKED:
                            batch.setColor(Color.LIME);
                            batch.draw(tileTextures.get(color), iso.x, iso.y - 32);
                            batch.setColor(col);
                            break;
                        case HOVERED:
                            batch.setColor(Color.SKY);
                            batch.draw(tileTextures.get(color), iso.x, iso.y - 32);
                            batch.setColor(col);
                            break;
                        default:
                            batch.draw(tileTextures.get(color), iso.x, iso.y - 32);
                            break;
                    }
                    batch.setColor(col.r, col.g, col.b, 1);
                }
            }
        }*/

        // rest of map
        for (int j = height - 1; j >= 0; j--) {
            for (int i = 0; i < width; i++) {
                // get cell
                TileMap.Cell cell = cells[i][j];
                String color = cell.hexColor;

                // render cell
                if (tileTextures.containsKey(color)) {
                    Vector2 iso = map.getTilePosition(i, j);
/*
                    // tiles right trans
                    if (j < py + 2 && j > py - 4 && i > px && i < px + 4 && map.isCellBlocked(i, j)) {
                        batch.setColor(col.r, col.g, col.b, .2f);
                    }
                    // tiles below trans
                    if (j < py && j > py - 4 && i > px - 4 && i <= px && map.isCellBlocked(i, j)) {
                        batch.setColor(col.r, col.g, col.b, .2f);
                    }
*/
                    //if (!color.equalsIgnoreCase("#ffffff")) {
                    switch (cell.state) {
                        case CLICKED:
                            batch.setColor(Color.LIME);
                            batch.draw(tileTextures.get(color), iso.x, iso.y - 32);
                            batch.setColor(col);
                            break;
                        case HOVERED:
                            batch.setColor(Color.SKY);
                            batch.draw(tileTextures.get(color), iso.x, iso.y - 32);
                            batch.setColor(col);
                            break;
                        default:
                            batch.draw(tileTextures.get(color), iso.x, iso.y - 32);
                            break;
                    }
                    batch.setColor(col.r, col.g, col.b, 1);
                    //}
                }

                // render cell objects
                for (MapObject object : cell.getObjects()) {

                    // Character
                    if (object instanceof Character) {

                    	if (object instanceof Character) {
                    		int frame = ((Character)object).frame;
                        	double angle = Math.toDegrees(object.angle);
                        	if (angle < 0) angle += 360;
                        	
                        	Gdx.app.log("Player",  "angle:  " + angle);
                        	
                        	if (angle > 270)
                        		batch.draw(playerTexturesRight[frame], object.position.x - 24, object.position.y - 8, 48, 96);
                        	else if (angle > 180)
                        		batch.draw(playerTexturesDown[frame], object.position.x - 24, object.position.y - 8, 48, 96);
                        	else if (angle > 90)
                        		batch.draw(playerTexturesLeft[frame], object.position.x - 24, object.position.y - 8, 48, 96);
                        	else
                        		batch.draw(playerTexturesUp[frame], object.position.x - 24, object.position.y - 8, 48, 96);
                        }

                    }
                    // Item
                    else if (object instanceof Item) {
                        batch.draw(block, object.position.x - 16, object.position.y, 32, 64);
                    }
                    // Map Doors
                    else if (object instanceof Door) {
                        Door door = (Door) object;
                        switch (door.type) {
                            case 12:
                                batch.draw(radio[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 11:
                                batch.draw(compY[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 10:
                                batch.draw(compX[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 9:
                                batch.draw(Captain[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 8:
                                batch.draw(doorCargo[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 7:
                                batch.draw(Jack[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 6:
                                batch.draw(doorPower[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 5:
                                batch.draw(doorPower[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 4:
                                batch.draw(doorCyro[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 3:
                                batch.draw(doorSpikes[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 2:
                                batch.draw(doorTextures[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            case 0:
                                batch.draw(doorYTextures[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                            default:
                                batch.draw(doorXTextures[door.state], object.position.x - 32, object.position.y - 32);
                                break;
                        }
                    }
                    // Pressure Plates
                    else if (object instanceof PressurePlate) {
                        //PressurePlate plate = (PressurePlate) object;
                        batch.draw(switchTextures[1], object.position.x - 32, object.position.y - 32);
                    }
                    // Switch
                    else if (object instanceof Switch) {
                        batch.draw(switchTextures[0], object.position.x - 32, object.position.y - 32);
                    }
                    // Item Switch
                    else if (object instanceof ItemSwitch) {

                        batch.setColor(Color.LIME);
                        batch.draw(switchTextures[0], object.position.x - 32, object.position.y - 32);
                        batch.setColor(col);

                    }
                    // Map Switcher
                    else if (object instanceof MapSwitcher) {
                        batch.setColor(Color.RED);
                        batch.draw(switchTextures[1], object.position.x - 16, object.position.y - 16, 32, 64);
                        batch.setColor(col);
                    }
                    // Story Point
                    else if (object instanceof StoryPoint) {

                    }
                    // Turret
                    else if (object instanceof Turret) {
                        postMapRenderObjects.add(object);

                        Turret turret = (Turret) object;
                        batch.draw(switchTextures[2], object.position.x - 32, object.position.y - 32);

                        int beamWidth = 1;
                        Color beamColor = Color.RED;
                        if (turret.firing) {
                            beamColor = Color.GREEN;
                            beamWidth = 4;
                        }
                        if (turret.active) {
	                        int srcFunc = batch.getBlendSrcFunc();
	                        int dstFunc = batch.getBlendDstFunc();
	                        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
	
	                        batch.setColor(beamColor.r, beamColor.g, beamColor.b, .5f);
	                        batch.draw(beamTextures[0], object.position.x - beamWidth / 2, object.position.y + 32, beamWidth / 2, 0, beamWidth, object.getDistance(turret.targetPosition, turret.position), 1, 1, object.getAngleDeg(turret.targetPosition, turret.position), 0, 0, beamTextures[0].getWidth(), beamTextures[0].getHeight(), false, false);
	                        batch.draw(beamTextures[1], object.position.x - beamWidth * 1.5F, object.position.y + 32, beamWidth * 1.5f, 0, beamWidth * 3, object.getDistance(turret.targetPosition, turret.position), 1, 1, object.getAngleDeg(turret.targetPosition, turret.position), 0, 0, beamTextures[1].getWidth(), beamTextures[1].getHeight(), false, false);
	                        batch.setBlendFunction(srcFunc, dstFunc);
	                        batch.setColor(col);
                        }

                    }
                    else {
                        switch (object.type) {
                            case 0:
                                batch.draw(img, object.position.x - 16, object.position.y, 32, 64);
                                break;
                            case 1:
                                batch.draw(block, object.position.x - 32, object.position.y - 16, 64, 128);
                                break;
                        }
                    }
                }

            }
        }

        for (MapObject object : postMapRenderObjects) {

            if (object instanceof Turret) {

                Turret turret = (Turret) object;


            }

        }
    }




}
