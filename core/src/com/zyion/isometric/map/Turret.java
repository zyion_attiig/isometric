package com.zyion.isometric.map;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Zyion on 9/01/2016.
 */
public class Turret extends MapObject {

    public Vector2 targetPosition;
    public boolean firing = false;
    public boolean active = false;

    public Turret(TileMap map, int tileX, int tileY, int type, boolean active) {
        super(map, tileX, tileY, type);
        this.blocking = true;
        this.targetPosition = position;
		this.active = active;
    }
    
    int totalTicks = 40;
    int ticks = 40;
    
    public void toggle() {
    	if (ticks == totalTicks) {
    		ticks = 0;
    		active = !active;
    	}
    }

    @Override
    public void update() {
        super.update();

        int tiles = 0;
        firing = false;
        
        if (ticks < totalTicks) {
        	ticks++;
        }
        
        if (active) {
	        switch (type) {
	            case 3: // down facing
	                loop : while (map.containsTile(tileX, tileY + (--tiles))) {
	                    TileMap.Cell cell = map.getCellAt(tileX, tileY + tiles);
	                    targetPosition = map.getTileCenterPosition(tileX, tileY + tiles);
	                    for (MapObject obj : cell.getObjects()) {
	                    	if (obj instanceof Character) {
	                    		((Character) obj).shot = true;
	                    	}
	                        firing = true;
	                        break loop;
	                    }
	                    if (map.isCellBlocked(tileX, tileY + tiles)) {
	                        break loop;
	                    }
	                }
	                break;
	            case 2: // left facing
	                loop : while (map.containsTile(tileX + (--tiles), tileY)) {
	                    TileMap.Cell cell = map.getCellAt(tileX + tiles, tileY);
	                    targetPosition = map.getTileCenterPosition(tileX + tiles, tileY);
	                    for (MapObject obj : cell.getObjects()) {
	                    	if (obj instanceof Character) {
	                    		((Character) obj).shot = true;
	                    	}
	                        firing = true;
	                        break loop;
	                    }
	                    if (map.isCellBlocked(tileX + tiles, tileY)) {
	                        break loop;
	                    }
	                }
	                break;
	            case 1: // up facing
	                loop : while (map.containsTile(tileX, tileY + (++tiles))) {
	                    TileMap.Cell cell = map.getCellAt(tileX, tileY + tiles);
	                    targetPosition = map.getTileCenterPosition(tileX, tileY + tiles);
	                    for (MapObject obj : cell.getObjects()) {
	                    	if (obj instanceof Character) {
	                    		((Character) obj).shot = true;
	                    	}
	                        firing = true;
	                        break loop;
	                    }
	                    if (map.isCellBlocked(tileX, tileY + tiles)) {
	                        break loop;
	                    }
	                }
	                break;
	            default: // right facing
	                loop : while (map.containsTile(tileX + (++tiles), tileY)) {
	                    TileMap.Cell cell = map.getCellAt(tileX + tiles, tileY);
	                    targetPosition = map.getTileCenterPosition(tileX + tiles, tileY);
	                    for (MapObject obj : cell.getObjects()) {
	                    	if (obj instanceof Character) {
	                    		((Character) obj).shot = true;
	                    	}
	                        firing = true;
	                        break loop;
	                    }
	                    if (map.isCellBlocked(tileX + tiles, tileY)) {
	                        break loop;
	                    }
	                }
	                break;
	        }
        }
        

    }
}
