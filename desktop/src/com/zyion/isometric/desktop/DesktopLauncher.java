package com.zyion.isometric.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.zyion.isometric.IsoGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = IsoGame.TITLE;
		config.width = IsoGame.WIDTH;
		config.height = IsoGame.HEIGHT;
		//config.foregroundFPS = 0;
		//config.backgroundFPS = 0;
		//config.vSyncEnabled = false;

		new LwjglApplication(new IsoGame(), config);
	}
}
